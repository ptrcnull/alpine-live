# alpine-live

a set of dockerfiles that transform into live squashfs-based alpine images

you have to be on alpine and have `linux-lts` installed.  
i don't make the rules.

usage:
```console
$ apk add linux-lts podman mkinitfs xorriso ripgrep
$ ./build.sh standard
$ ./build.sh xorg 3.17
```
