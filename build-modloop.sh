#!/bin/sh

ARCH=x86_64 # XD
KVER=$1
MODLOOP=modloop
ROOTFS=/

set -eu

cache_file="/tmp/alpine-live-modloop-cache-$KVER"

if [ -f "$cache_file" ]; then
    cp "$cache_file" content/modloop-lts
    exit 0
fi

mkdir -p modloop/modules
cp -a /lib/modules/$KVER modloop/modules/

# unpack all modules to avoid double compression
find modloop/modules \
	  -name \*.ko.gz -exec gunzip {} + \
	-o -name \*.ko.xz -exec unxz {} + \
	-o -name \*.ko.zst -exec unzstd --rm {} + \
	-o ! -name ''

find $ROOTFS/lib/modules -type f -name "*.ko*" | xargs modinfo -k $KVER -F firmware | sort -u | while read FW; do
	for f in "$ROOTFS"/lib/firmware/$FW; do
		if ! [ -e "$f" ]; then
			continue
		fi
		install -pD "$f" "$MODLOOP/modules/firmware/${f#*/lib/firmware}"
		# copy also all potentially associated files
		for _file in "${f%.*}".*; do
			install -pD "$_file" "$MODLOOP/modules/firmware/${_file#*/lib/firmware/}"
		done
	done
done

# wireless regulatory db
if [ -e "$ROOTFS"/lib/modules/*/kernel/net/wireless/cfg80211.ko* ]; then
	for _regdb in "$ROOTFS"/lib/firmware/regulatory.db*; do
		[ -e "$_regdb" ] && install -pD "$_regdb" "$MODLOOP"/modules/firmware/"${_regdb##*/}"
	done
fi

# include bluetooth firmware in modloop
if [ -e "$ROOTFS"/lib/modules/*/kernel/drivers/bluetooth/btbcm.ko* ] && \
	[ -e "$ROOTFS"/lib/firmware/brcm/*.hcd ]; then
	for _btfw in "$ROOTFS"/lib/firmware/brcm/*.hcd; do
		install -pD "$_btfw" \
			"$MODLOOP"/modules/firmware/brcm/"${_btfw##*/}"
	done
fi

case $ARCH in
	armhf) mksfs="-Xbcj arm" ;;
	armv7|aarch64) mksfs="-Xbcj arm,armthumb" ;;
	x86|x86_64) mksfs="-Xbcj x86" ;;
	*) mksfs=
esac
mksquashfs $MODLOOP content/modloop-lts -comp xz -exit-on-error -all-root $mksfs
cp content/modloop-lts "$cache_file"
