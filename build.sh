#!/bin/sh

set -eux

for cmd in podman mkinitfs xorrisofs rg tar2sqfs; do
	if ! command -v $cmd > /dev/null; then
		echo "command $cmd not found"
		exit 1
	fi
done

for file in \
	/usr/share/limine/limine-cd.bin \
	/usr/share/limine/limine-cd-efi.bin \
	/usr/share/limine/limine.sys; do
	if ! [ -e "$file" ]; then
		echo "file $file not found"
		exit 1
	fi
done

outdir=$PWD
startdir="$(dirname "$(realpath "$0")")"

if [ ! -f "Dockerfile.$1" ]; then
	echo "preset $1 not found"
	exit 1
fi

alpine_version="edge"
if [ "$#" -gt 1 ]; then
	alpine_version="$2"
fi

build_image() {
	grep "^FROM alpine-live-rootfs-" Dockerfile.$1 | cut -d- -f4 | cut -d: -f1 | while read -r dep; do
		build_image "$dep"
	done
	podman build --build-arg="version=$alpine_version" -t alpine-live-rootfs-$1:$alpine_version -f Dockerfile.$1
}

build_image "$1"

workdir=$(mktemp -d /tmp/live.XXXXXX)
cd $workdir

# create folder structure
mkdir -p content/boot/grub

container_id=$(podman create alpine-live-rootfs-$1:$alpine_version)
podman export $container_id | tar2sqfs content/melchior.sfs
podman rm $container_id

kernel_ver=$(file /boot/vmlinuz-lts | rg -r '$1' -o 'version (.*?) ')
"$startdir"/build-modloop.sh $kernel_ver

# === prepare initramfs ==
_features="ata base bootchart cdrom ext4 mmc nvme raid scsi squashfs usb virtio"
mkinitfs -i "$startdir/init" -F "$_features" -o content/boot/initramfs $kernel_ver

# === assemble the iso ===
cat > content/limine.cfg <<-EOF
DEFAULT_ENTRY=1
TIMEOUT=5
VERBOSE=yes

:Alpine Linux $alpine_version $1
	PROTOCOL=linux
	KERNEL_PATH=boot:///boot/vmlinuz
	MODULE_PATH=boot:///boot/initramfs
	CMDLINE=modules=loop,squashfs,sd-mod,usb-storage console=tty0
EOF

cp /boot/vmlinuz-lts content/boot/vmlinuz
cp /usr/share/limine/limine-cd.bin content/boot/limine-cd.bin
cp /usr/share/limine/limine-cd-efi.bin content/boot/limine-cd-efi.bin
cp /usr/share/limine/limine.sys content/boot/limine.sys

xorriso \
	-as mkisofs \
	-output "$outdir"/output-$1-$alpine_version.iso \
	-b boot/limine-cd.bin \
	-sysid LINUX \
	-volid "alpine-$1 $alpine_version x86_64" \
	-no-emul-boot \
	-boot-load-size 4 \
	-boot-info-table \
	--efi-boot boot/limine-cd-efi.bin \
	-efi-boot-part \
	--efi-boot-image \
	--protective-msdos-label \
	content
limine-deploy "$outdir"/output-$1-$alpine_version.iso

rm -rf "$workdir"
